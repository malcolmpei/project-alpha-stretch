from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm, TextInput, EmailInput


class AccountForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)
    password_confirmation = forms.CharField(
        max_length=150, widget=forms.PasswordInput
    )


class EditProfileForm(ModelForm):
    class Meta:
        model = User
        fields = (
            'email',
            'first_name',
            'last_name'
        )
        widgets = {
            'email': EmailInput(attrs={'placeholder': 'name@server.com'}),
            'first_name': TextInput(attrs={'placeholder': 'John'}),
            'last_name': TextInput(attrs={'placeholder': 'Smith'})
        }
