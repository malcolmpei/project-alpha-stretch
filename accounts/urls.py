from django.urls import path
from accounts.views import account_login, account_logout, account_signup, edit_profile, overview


urlpatterns = [
    path("login/", account_login, name="login"),
    path("logout/", account_logout, name="logout"),
    path("signup/", account_signup, name="signup"),
    path('edit/', edit_profile, name="edit_profile"),
    path('overview/', overview, name='overview')
]
