from django.shortcuts import render, redirect
from accounts.forms import AccountForm, SignUpForm, EditProfileForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from companies.models import Company
from projects.models import Project
from tasks.models import Task


def account_login(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("list_projects")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "accounts/login.html", context)


def account_logout(request):
    logout(request)
    return redirect("login")


def account_signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                )
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error("password", "the passwords do not match")
    else:
        form = SignUpForm()
    context = {"form": form}
    return render(request, "accounts/signup.html", context)


@login_required
def edit_profile(request):
    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('list_companies')
    else:
        form = EditProfileForm(instance=request.user)
    context = {"form": form, "user": request.user}
    return render(request, 'accounts/edit_profile.html', context)


@login_required
def overview(request):
    user = request.user
    companies = Company.objects.all()
    projects = Project.objects.filter(owner=request.user)
    tasks = Task.objects.filter(assignee=request.user)
    context = {"user": user, "companies": companies, "projects": projects, "tasks": tasks}
    return render(request, 'accounts/overview.html', context)
