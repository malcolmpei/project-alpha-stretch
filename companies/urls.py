from django.urls import path
from companies.views import list_companies, search_project, create_company, edit_company, delete_company


urlpatterns = [
    path('', list_companies, name='list_companies'),
    path('search/', search_project, name='search_project'),
    path('create/', create_company, name='create_company'),
    path('<int:id>/edit/', edit_company, name='edit_company'),
    path('<int:id>/delete/', delete_company, name='delete_company')
]
