from django.db import models
from django.contrib.auth.models import User


class Company(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    owner = models.ForeignKey(
        User,
        related_name='companies',
        on_delete=models.CASCADE,
        null=True,
        default=User
    )
    logo = models.URLField(blank=True, null=True, default='https://freepngimg.com/thumb/logo/88500-text-awesome-question-mark-font-symbol.png')

    def __str__(self):
        return self.name
