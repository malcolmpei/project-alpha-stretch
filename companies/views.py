from django.shortcuts import render, redirect, get_object_or_404
from companies.models import Company
from projects.models import Project
from django.db.models import Q
from companies.forms import CompanyForm
from django.contrib.auth.decorators import login_required


@login_required
def list_companies(request):
    companies = Company.objects.all()
    context = {"companies": companies}
    return render(request, 'companies/list_company.html', context)


@login_required
def search_project(request):
    results = []
    if request.method == 'GET':
        query = request.GET.get('search')
        if query == '':
            query = 'None'
        results = Project.objects.filter(Q(name__icontains=query))
    context = {'query': query, 'results': results}
    return render(request, 'companies/search_project.html', context)


@login_required
def create_company(request):
    if request.method == 'POST':
        form = CompanyForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('list_companies')
    else:
        form = CompanyForm()
    context = {'form': form}
    return render(request, 'companies/create_company.html', context)


@login_required
def edit_company(request, id):
    company = get_object_or_404(Company, id=id)
    if request.method == 'POST':
        form = CompanyForm(request.POST, instance=company)
        if form.is_valid():
            form.save()
            return redirect('list_companies')
    else:
        form = CompanyForm(instance=company)
    context = {"form": form, "company": company}
    return render(request, 'companies/edit_company.html', context)


@login_required
def delete_company(request, id):
    company = get_object_or_404(Company, id=id)
    if request.method == 'POST':
        company.delete()
        return redirect('list_companies')
    context = {"company": company}
    return render(request, 'companies/delete_company.html', context)
