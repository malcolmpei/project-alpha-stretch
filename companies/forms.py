from django.forms import ModelForm, TextInput, Textarea
from companies.models import Company


class CompanyForm(ModelForm):
    class Meta:
        model = Company
        fields = ("name", "description", "logo", "owner")
        widgets = {
            'name': TextInput(
                attrs={'placeholder': 'company name...'}),
            'description': Textarea(
                attrs={'placeholder': 'describe your company...'}),
                }
