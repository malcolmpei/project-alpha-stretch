from django.urls import path
from tasks.views import create_task, my_tasks, show_task, show_chart, search_task


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", my_tasks, name="show_my_tasks"),
    path('<int:id>', show_task, name='task_detail'),
    path('chart/', show_chart, name='show_chart'),
    path('search/', search_task, name='search_task')
]
