from django.forms import ModelForm, TextInput, Textarea
from django import forms
from tasks.models import Task


class DateInput(forms.DateInput):
    input_type = 'date'


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
            "is_completed",
            "note"
            ]
        widgets = {
            'name': TextInput(attrs={
                'class': 'form-control',
                'style': 'max-width: 300px;',
                'placeholder': 'task name...'
            }),
            'start_date': DateInput(),
            'due_date': DateInput(),
            'note': Textarea(attrs={
                'placeholder': 'notes...'
                })
            }
