from django.shortcuts import render, redirect, get_list_or_404
from tasks.forms import TaskForm
from projects.models import Project
from django.contrib.auth.decorators import login_required
from tasks.models import Task
import pandas as pd
from plotly.offline import plot
import plotly.express as px
from django.db.models import Q


@login_required
def create_task(request):
    projects = Project.objects.filter(owner=request.user)
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {"form": form, "projects": projects}
    return render(request, "tasks/create_task.html", context)


@login_required
def my_tasks(request):
    projects = Project.objects.filter(owner=request.user)
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks, "projects": projects}
    return render(request, "tasks/my_tasks.html", context)


@login_required
def show_task(request, id):
    task = get_list_or_404(Task, id=id)
    context = {"task": task}
    return render(request, 'tasks/task_detail.html', context)


@login_required
def show_chart(request):
    tasks = Task.objects.filter(assignee=request.user)
    data = [
        {
            'task': i.name,
            'start': i.start_date,
            'end': i.due_date,
            'assignee': i.assignee,
            'completed': i.is_completed
            } for i in tasks
            ]
    if data:
        dataframe = pd.DataFrame(data)
        figure = px.timeline(
            dataframe, x_start='start', x_end='end', y='task', color='completed'
        )
        figure.update_layout(
            font_family='"Martian Mono", monospace',
            paper_bgcolor='#CDCACC',
            plot_bgcolor='#CDCACC',
            legend_orientation='h',
            showlegend=False,
            )
        figure.update_yaxes(autorange='reversed')
        gantt_plot = plot(figure, output_type='div')
        context = {'plot_div': gantt_plot, 'tasks': tasks}
    else:
        context = {'tasks': tasks}
    return render(request, 'tasks/chart.html', context)


@login_required
def search_task(request):
    results = []
    if request.method == 'GET':
        query = request.GET.get('search')
        if query == '':
            query = 'None'
        results = Task.objects.filter(Q(name__icontains=query))
    context = {'query': query, 'results': results}
    return render(request, 'tasks/search_task.html', context)
