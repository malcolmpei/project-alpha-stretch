from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from tasks.models import Task
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    tasks = Task.objects.all()
    context = {"projects": projects, "tasks": tasks}
    return render(request, "projects/list.html", context)


@login_required
def list_details(request, id):
    details = get_object_or_404(Project, id=id)
    projects = Project.objects.filter(owner=request.user)
    context = {"details": details, "projects": projects}
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    projects = Project.objects.filter(owner=request.user)
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form, "projects": projects}
    return render(request, "projects/create.html", context)


@login_required
def edit_task(request, id):
    projects = Project.objects.filter(owner=request.user)
    task = get_object_or_404(Task, id=id)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect('show_my_tasks')
    else:
        form = TaskForm(instance=task)
    context = {"form": form, "task": task, "projects": projects}
    return render(request, "tasks/edit_task.html", context)


@login_required
def edit_project(request, id):
    projects = Project.objects.filter(owner=request.user)
    project = get_object_or_404(Project, id=id)
    if request.method == "POST":
        form = ProjectForm(request.POST, instance=project)
        if form.is_valid():
            form.save()
            return redirect('list_projects')
    else:
        form = ProjectForm(instance=project)
    context = {"form": form, "project": project, "projects": projects}
    return render(request, "projects/edit_project.html", context)


@login_required
def delete_project(request, id):
    projects = Project.objects.filter(owner=request.user)
    project = get_object_or_404(Project, id=id)
    if request.method == "POST":
        project.delete()
        return redirect('list_projects')
    context = {"project": project, "projects": projects}
    return render(request, 'projects/delete_project.html', context)


@login_required
def delete_task(request, id):
    projects = Project.objects.filter(owner=request.user)
    task = get_object_or_404(Task, id=id)
    if request.method == "POST":
        task.delete()
        return redirect('list_projects')
    context = {"task": task, "projects": projects}
    return render(request, 'tasks/delete_task.html', context)
