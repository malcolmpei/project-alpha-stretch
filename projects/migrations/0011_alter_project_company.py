# Generated by Django 4.1.4 on 2022-12-16 17:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("companies", "0007_alter_company_owner"),
        ("projects", "0010_alter_project_company"),
    ]

    operations = [
        migrations.AlterField(
            model_name="project",
            name="company",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="projects",
                to="companies.company",
            ),
        ),
    ]
