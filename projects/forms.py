from django.forms import ModelForm, TextInput, Textarea
from projects.models import Project


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ("name", "description", "company", "owner")
        widgets = {
            'name': TextInput(attrs={
                'class': 'form-control',
                'style': 'max-width: 500px;',
                'placeholder': 'project name'
            }),
            'description': Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'describe your project'
            })
        }
