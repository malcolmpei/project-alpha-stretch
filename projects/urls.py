from django.urls import path
from projects.views import list_projects, list_details, create_project, edit_task, edit_project, delete_project, delete_task


urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", list_details, name="show_project"),
    path("create/", create_project, name="create_project"),
    path('<int:id>/edit_task/', edit_task, name='edit_task'),
    path('<int:id>/edit_project/', edit_project, name='edit_project'),
    path('<int:id>/delete_project/', delete_project, name='delete_project'),
    path('<int:id>/delete_task', delete_task, name='delete_task')
]
